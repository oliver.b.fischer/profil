# Selbstdarstellung Oliver B. Fischer

Portrait und Selbstdarstellung von Oliver B. Fischer. Die
hier bereit gestellten Informationen und Bilder 
können von Organisatoren von Usergruppen und Konferenzen
frei verwendet werden, sofern der Verwendungszweck 
in Bezug zu meinen öffentlichen Aktivitäten steht.

Oliver B. Fischer  
Berlin, 19. März 2015